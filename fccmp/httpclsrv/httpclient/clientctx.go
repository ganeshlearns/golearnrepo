package main

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"time"
)

func main() {
	fmt.Println("from the client")

	//customized client with timeout defined
	client := &http.Client{
		Timeout: 10000 * time.Millisecond,
	}
	//getting a request
	req, err := http.NewRequestWithContext(context.Background(), http.MethodGet, "http://localhost:8080/hello", nil)
	if err != nil {
		panic(err)
	}
	//context extracted from the request
	ctx := req.Context()
	fmt.Println("context from the req extracted\t", ctx)
	//context added with timeout
	ctx2, cancel := context.WithTimeout(ctx, 1000*time.Millisecond)
	defer cancel()
	fmt.Println("context added with timeout\t", ctx2)

	req = req.WithContext(ctx2)

	//sending a request from the client and getting a response
	res, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		panic(err)
	}
	fmt.Println("the body of the response\t", string(body))
	//response status
	fmt.Println("the response status is\t", res.Status)
	//response header
	fmt.Println("the response header is\t", res.Header)
}
