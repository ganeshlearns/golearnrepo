package main

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"time"
)

type contextKey int

const key contextKey = 25

func main() {

	fmt.Println("from the client")

	//customized client with timeout defined
	client := &http.Client{
		Timeout: 10000 * time.Millisecond,
	}

	ctx := context.WithValue(context.Background(), key, "Go")
	//getting a request
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, "http://localhost:8080/hello", nil)
	if err != nil {
		panic(err)
	}
	//context extracted from the request
	//ctx := req.Context()
	//fmt.Println("context from the req extracted\t", ctx)
	//context added with  value

	fmt.Println("context added with timeout\t", ctx)

	req = req.WithContext(ctx)

	//sending a request from the client and getting a response
	res, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		panic(err)
	}
	fmt.Println("the body of the response\t", string(body))
	//response status
	fmt.Println("the response status is\t", res.Status)
	//response header
	fmt.Println("the response header is\t", res.Header)
}
