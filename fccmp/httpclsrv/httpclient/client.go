package main

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"time"
)

func main() {
	fmt.Println("from the client")
	//customized client with timeout defined
	client := &http.Client{
		Timeout: 10000 * time.Millisecond,
	}
	//getting a request
	req, err := http.NewRequestWithContext(context.Background(), http.MethodGet, "http://localhost:8080/hello", nil)
	if err != nil {
		panic(err)
	}
	/*//adding a header to the request
	req.Header.Add("h1", "http://localhost:8080")*/
	//sending a request from the client and getting a response
	res, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		panic(err)
	}
	fmt.Println("the body of the response\t", string(body))
	//response status
	fmt.Println("the response status is\t", res.Status)
	//response header
	fmt.Println("the response header is\t", res.Header)
}
