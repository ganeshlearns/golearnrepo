package main

import (
	"fmt"
	"net/http"
	"time"
)

//handler declaration
type MyHandler struct{}

func (mh MyHandler) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	{
		rw.Write([]byte("hello from the basic server"))
	}
}

func main() {
	fmt.Println("from the server")
	//creating a server
	server := http.Server{
		Addr:         ":8080",
		ReadTimeout:  1000 * time.Millisecond,
		WriteTimeout: 10000 * time.Millisecond,
		IdleTimeout:  10000 * time.Millisecond,
		Handler:      MyHandler{},
	}
	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}
