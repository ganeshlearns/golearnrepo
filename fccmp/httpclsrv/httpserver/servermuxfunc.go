package main

import (
	"fmt"
	"net/http"
	"time"
)

func serveHTTP(rw http.ResponseWriter, req *http.Request) {
	{
		/*//context from the request
		ctx := req.Context()
		fmt.Println("the context in the server is ", ctx)*/

		rw.Write([]byte("mux hello using mux as handler while server creation"))
	}
}

func main() {
	fmt.Println("from the server")
	//getting a servemux
	mux := http.NewServeMux()
	//creating a server
	server := http.Server{
		Addr:         ":8080",
		ReadTimeout:  1000 * time.Millisecond,
		WriteTimeout: 10000 * time.Millisecond,
		IdleTimeout:  10000 * time.Millisecond,
		Handler:      mux, //MyHandler{},
	}
	//mux  handling the request with path
	mux.HandleFunc("/hello", serveHTTP)
	mux.HandleFunc("/hello2", serveHTTP)
	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}
