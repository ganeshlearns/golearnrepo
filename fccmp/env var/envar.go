package main

import (
	"fmt"
	"os"
	"strings"
)

func main() {
	fmt.Println("from the env var example")
	//setting an env var
	os.Setenv("envar1", "envarValue")
	//getting all the env var
	for _, envar := range os.Environ() {
		/*//gives a slice splitting each env var and value
		envarStr := strings.Split(envar, "=")
		//printing each env var and value separately
		for _, ns := range envarStr {
			fmt.Println(ns)
		}*/
		//gets the env var and value into a slice
		envarStr := strings.SplitN(envar, "=", 2)
		//prints the env var name only
		fmt.Println(envarStr[0])
	}
}
