package main

import (
	"fmt"
)

func main() {
	var p *myStruct
	p = &myStruct{name: "ganesh"}
	fmt.Println(p)
	fmt.Println((*p).name)
	fmt.Println(p.name) //compiler gives this for convenience
}

//struct declaration
type myStruct struct {
	name string
}
