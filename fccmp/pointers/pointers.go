package main

import (
	"fmt"
)

func main() {
	fmt.Println("pointers example")
	var a int = 10
	//simple pointer declaration and initiailization
	var b *int = &a
	c := &b
	fmt.Println("a,b,c", a, b, c)
	a = 60
	fmt.Println("a,b,c", a, *b, *c)
}
