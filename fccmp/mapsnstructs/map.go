package main

import (
	"fmt"
)

func main() {
	fmt.Println("this is an example of map")
	//declaring a map and initialising it
	m1 := map[string]int{
		"key1": 10,
		"key2": 20,
		"key3": 30,
	}
	//printing the map
	fmt.Println("printing the map with the initial values", m1)
	fmt.Println("value of the key1", m1["key1"])
	//adding a key to the map
	m1["key4"] = 40
	fmt.Println("After adding  key4 ", m1)
	//deleting a key to the map
	delete(m1, "key4")
	fmt.Println("After deleting the key4", m1)
	pop, ok := m1["key4"]
	fmt.Println("does the key exists and the value\t", pop, ok)
	fmt.Println("length of the map is ", len(m1))
}
