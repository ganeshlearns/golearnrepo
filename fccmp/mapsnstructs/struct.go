package main

import (
	"fmt"
)

func main() {
	fmt.Println("from the structs")
	//declaring a struct
	type formalStruct struct {
		name    string
		number  int
		address []string
	}
	//giving values to the struct
	fstruct1 := formalStruct{
		name:    "ganesh",
		number:  1234,
		address: []string{"hyderabad", "andhrapradesh", "india"},
	}
	fmt.Println(fstruct1)
	fmt.Println(fstruct1.name)
	fmt.Println(fstruct1.number)
	fmt.Println(fstruct1.address)
	fmt.Println(fstruct1.address[1])
	//anonymous struct
	astruct := struct{ name string }{name: "ganesh"}
	fmt.Println(astruct)
}
