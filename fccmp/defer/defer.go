package main

import (
	"fmt"
)

func main() {
	defer fmt.Println("in the defer1")
	defer fmt.Println("in the defer2")
	defer fmt.Println("in the defer3")
	defer fmt.Println("in the defer4")
}
