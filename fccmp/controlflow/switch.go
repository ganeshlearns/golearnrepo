package main

import (
	"fmt"
)

func main() {
	fmt.Println("from switch example")
	//switch declaration
	switch false {
	case false:
		fmt.Println("from the case1 of the switch")
		fallthrough
	case true:
		fmt.Println("from the case2 of the switch")
		break
	default:
		fmt.Println("from the default of the switch")
		break
	}
	//one more switch declaration
	switch i := 2 + 3; i {
	case 1, 2, 4:
		fmt.Println("from the case 1, 2, 4 of the switch")
		//fallthrough//logic less
	case 3, 5, 6:
		fmt.Println("from the case 3, 5, 6 of the switch")
		break
		//default:
		//fmt.Println("from the default of the second switch")
		//break
	}
	//untagged switch declaration
	j := 2
	switch {
	case j < 10:
		fmt.Println("from the case j<10 of the switch")
		//fallthrough
	case j < 20:
		fmt.Println("from the case j<20 of the switch")
		break
		//default:
		//fmt.Println("from the default of the second switch")
		//break
	}
	//typed switch declaration
	var k interface{} = [5]int{}
	switch k.(type) {
	case int:
		fmt.Println("from the case int of the typed switch")
		//fallthrough
	case float64:
		fmt.Println("from the case float64 of the typed switch")
		break
	case [5]int:
		fmt.Println("from the case [5]int{} of the typed switch")
		//default:
		//fmt.Println("from the default of the second switch")
		//break
	}
}
