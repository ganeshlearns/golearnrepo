package main

import (
	"fmt"
)

func main() {
	fmt.Println("from if example")
	//if declaration
	if false {
		fmt.Println("in  if")
	} else if true {
		fmt.Println("in else if1 ")
	} else if true {
		fmt.Println("in else if2 ")
	} else {
		fmt.Println("in else")
	}
}
