package myclient

import (
	"fmt"
	"log"
	"net/http"
)

func ClientMain() {
	fmt.Println("from the client")
	resp, err := http.Get("http://localhost:8080/hello")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(resp)
	fmt.Println(resp.Status)
	fmt.Println(resp.Proto)
}
