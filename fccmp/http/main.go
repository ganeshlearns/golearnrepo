package main

import (
	"fmt"
	"mhttp/myclient"
	"mhttp/myserver"
	"sync"
)

var wg sync.WaitGroup

func main() {
	fmt.Println("from the main method")
	wg.Wait()
	//wg.Add(1)
	go myserver.ServerMain()
	//time.Sleep(10 * time.Second)

	myclient.ClientMain()

}
