package myserver

import (
	"fmt"
	"log"
	"net/http"
	"sync"
)

var wg sync.WaitGroup

func reqHandler(rw http.ResponseWriter, req *http.Request) {
	fmt.Println("from the reqHandler func")
	i, err := rw.Write([]byte("body of response"))
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(i)

}

func ServerMain() {
	fmt.Println("from the server")
	http.HandleFunc("/hello", reqHandler)
	err := http.ListenAndServe("localhost:8080", nil)
	if err != nil {
		panic(err)
	}

	wg.Done()

}
