package main

import (
	"fmt"
)

func main() {
	var m int16 = 20

	//typed constant declaration
	const c int = 50
	k := c + int(m)

	//untyped constant declaration
	const c1 = 39
	r := c1 + m

	//printing the output
	fmt.Println(k)
	fmt.Println(r)
}
