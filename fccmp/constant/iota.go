package main

import (
	"fmt"
)

//using iota
const (
	c = iota
	d = iota + 1
	e = iota
)

func main() {

	//printing the output
	fmt.Println(c)
	fmt.Println(d)
	fmt.Println(e)
}
