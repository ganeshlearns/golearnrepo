package main

import (
	"fmt"
)

func main() {
	//declaring and initialising a boolean
	var b1 bool = true
	var b2 bool
	//evaluates and adds corresponding bool value to i
	i := 1 == 3
	fmt.Printf("%v, %T\n", b1, b1)
	fmt.Printf("%v, %T\n", i, i)
	fmt.Printf("initial value for a boolean\t %v, %T\n", b2, b2)
}
