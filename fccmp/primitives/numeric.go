package main

import (
	"fmt"
)

func main() {
	//declaring and initialising a int
	var b1 int = 100
	var b2 int8 = 9
	//evaluates and adds corresponding bool value to i
	i := b1 + int(b2)
	fmt.Printf("%v, %T\n", b1, b1)
	fmt.Printf("%v, %T\n", i, i)
	fmt.Printf("%v, %T\n", b2, b2)
}
