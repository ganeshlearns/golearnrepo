package main

import (
	"fmt"
)

func main() {
	var s string = "first string"
	b := []byte(s)
	fmt.Println(string(s[3]) + string(s[1]))
	fmt.Println(b)
	fmt.Println(string(b))
}
