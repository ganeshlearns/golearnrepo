package main

import (
	"fmt"
)

func main() {
	fmt.Println("from the interface example")

	//using structs
	var iface fInterface = mStruct{number: 22}
	iface.fintMethod()

	//using type alias
	var newN nNum = nNum(99)
	var iface2 fInterface = &newN
	iface2.fintMethod()
}

//declaring an interface
type fInterface interface {
	fintMethod()
}

//declaring a struct
type mStruct struct {
	number int
}

//declaring a method
func (ms mStruct) fintMethod() {
	fmt.Println("from the method implementing the interface", ms.number)
}

//declaring a type alias
type nNum int

//declaring a method
func (ms nNum) fintMethod() {
	fmt.Println("from the method implementing the interface", ms)
}
