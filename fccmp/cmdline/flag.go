package main

import (
	"fmt"
	//cmd line flag can be accessed using this pkg
	"flag"
)

func main() {
	fmt.Println("cmd line flag example")
	//flag of int with default value of 33
	intV := flag.Int("intflag", 33, "this is a int flag")
	//flag of float with default value of 9.0
	fV := flag.Float64("floatflag", 9.0, "float flag defined here")
	//flag of string with default value of "stringflagvalue"
	sV := flag.String("stringflag", "stringflagvalue", "string flag defined here")
	flag.Parse()
	fmt.Println("intflag:", *intV)
	fmt.Println("flaotflag", *fV)
	fmt.Println("stringflag", *sV)
}

/*will be using go build  to demonstrate it better
$./flag -h -----gives the default values*/
