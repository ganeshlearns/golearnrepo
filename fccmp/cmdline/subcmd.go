package main

import (
	"fmt"
	"os"

	//cmd line flag can be accessed using this pkg
	"flag"
)

func main() {
	fmt.Println("cmd line  sub cmd  example")
	//declaring  new flagset
	fs := flag.NewFlagSet("subcmd1", flag.ExitOnError)
	//getting a flag from the flagset
	st1 := fs.String("flag1", "value1", "flag1 is set for fs")
	nt1 := fs.Int("flag2", 0, "flag2 is set for fs")
	//parsing the flags
	fs.Parse(os.Args[2:])
	fmt.Println("flag1:", *st1)
	fmt.Println("flag2:", *nt1)
	fmt.Println("remaining args", fs.Args())
}

/*will be using go build  to demonstrate it better
$./flag -h -----gives the default values*/
