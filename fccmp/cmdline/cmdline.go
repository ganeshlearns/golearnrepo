package main

import (
	"fmt"
	//cmd line args can be accessed using this pkg
	"os"
)

func main() {
	fmt.Println("cmd line args example")
	//gives the slice of cmd line args
	a := os.Args
	fmt.Println(a)
	fmt.Println(a[1:])
	fmt.Println(a[:3])
}

//will be using go build  to demonstrate it better
