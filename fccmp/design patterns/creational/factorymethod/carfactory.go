package main

import "fmt"

//var pointer to car struct defined
var c *car

//getting the cars as per the string
func getCars(s string) cInf {
	if s == "maruti" {
		c = &car{
			model: "maruti",
			//seater: 4,
		}
	}
	if s == "suv" {
		c = &car{
			model: "suv",
			//seater: 6,
		}
	}
	fmt.Println("the appropriate car is\t", c)
	return c
}
