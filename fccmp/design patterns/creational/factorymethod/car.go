package main

//car  struct defined
type car struct {
	model  string
	seater int
}

//car interface defined here
type cInf interface {
	setModel(s string)
	getModel() string
	setSeater(i int)
	getSeater() int
}

//the set method for the model is defined here
func (c *car) setModel(s string) {
	c.model = s
}

//the get method for the model is defined here
func (c *car) getModel() string {
	return c.model
}

//the set method for the seater is defined here
func (c *car) setSeater(i int) {
	c.seater = i
}

//the get method for the seater is defined here
func (c *car) getSeater() int {
	return c.seater
}
