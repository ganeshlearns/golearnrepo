package main

type tableStr struct {
	modelName string
}

type tableIntf interface {
	getModelName() string
}

func(t tableStr) getModelName() string{
	return t.modelName
}
