package main

import "fmt"

type abcOfficeStr struct{}
type xyzOfficeStr struct{}

type officeIntf interface {
	getTable() tableIntf
	getChair() chairIntf
}

func (as abcOfficeStr) getTable() tableIntf {
	return abcTableStr{
		tableStr: tableStr{
			modelName: "abc table",
		},
	}
}

func (as abcOfficeStr) getChair() chairIntf {
	return abcChairStr{
		chairStr: chairStr{
			chModelName: "abc chair",
		},
	}
}

func (as xyzOfficeStr) getTable() tableIntf {
	return abcTableStr{
		tableStr: tableStr{
			modelName: "xyz table",
		},
	}
}

func (as xyzOfficeStr) getChair() chairIntf {
	return abcChairStr{
		chairStr: chairStr{
			chModelName: "xyz chair",
		},
	}
}

func getFurniture(s string) (officeIntf, error) {
	if s == "abc" {
		return abcOfficeStr{}, nil
	}
	if s == "xyz" {
		return xyzOfficeStr{}, nil
	}
	return nil, fmt.Errorf("erorrrr")

}
