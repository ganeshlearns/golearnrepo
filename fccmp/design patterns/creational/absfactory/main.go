package main

import "fmt"

func main() {
	fmt.Println("from the main")
	str, err := getFurniture("xyz")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(str.getTable().getModelName())
	fmt.Println(str.getChair().getChModelName())
}
