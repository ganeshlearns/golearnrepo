package main

type chairStr struct{
	chModelName string
}

type chairIntf interface{
	getChModelName() string
}

func(c chairStr) getChModelName() string{
	return c.chModelName
}