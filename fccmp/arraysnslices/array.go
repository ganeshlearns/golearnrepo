package main

import (
	"fmt"
)

func main() {
	fmt.Println("from the array app")
	//declaring and initialising an array
	var arr1 [3]int = [...]int{1, 1, 1}
	//arr1 = [...]int{1, 2, 3}
	fmt.Println(arr1)
	arr1[2] = 22
	arr2 := arr1
	arr2[2] = 9
	fmt.Println(arr1)
	fmt.Println(len(arr1))
	//shows copy by value
	fmt.Println(arr2)
}
