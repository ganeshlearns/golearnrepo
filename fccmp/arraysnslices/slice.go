package main

import (
	"fmt"
)

func main() {
	fmt.Println("from the slice app")
	//declaring and initialising an array
	arr1 := [...]int{1, 1, 1}
	fmt.Println("array is ", arr1)
	fmt.Println("length of the array is", len(arr1))
	//declaring and initialising a slice
	sarr1 := arr1[:]
	fmt.Println("slice sarr1 is", sarr1)

	sarr1[2] = 9

	fmt.Println("After changing the slice, array now is", arr1)

	fmt.Println("length of the slice is", len(sarr1))
	fmt.Println("capacity of the slice is", cap(sarr1))
	//deals with the reference
	fmt.Println("the  slice sarr1 is ", sarr1)

	//using make func for making slices
	sm1 := make([]int, 5, 50)
	fmt.Println(sm1)
	fmt.Println("length of the slice is", len(sm1))
	fmt.Println("capacity of the slice is", cap(sm1))

	sm1 = append(sm1, 18)
	//sarr1 = append(sarr1, 333)
	fmt.Println("length of the slice is", len(sm1))
	fmt.Println(sm1)
	//fmt.Println(sarr1)
}
