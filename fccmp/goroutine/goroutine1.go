package main

import (
	"fmt"
	"sync"
)

var wg sync.WaitGroup

func main() {
	fmt.Println("from the main in goroutine example")
	i := 100
	wg.Add(1)

	go func(i int) {
		fmt.Println("from the goroutine example", i)
		wg.Done()
	}(i)
	i = 300
	wg.Wait()
	//time.Sleep(time.Millisecond)
}
