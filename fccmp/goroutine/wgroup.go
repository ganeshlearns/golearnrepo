package main

import (
	"fmt"
	"sync"
)

var wg sync.WaitGroup
var m sync.Mutex

func main() {
	fmt.Println("from the main method")
	for i := 0; i < 10; i++ {
		wg.Add(2)
		m.Lock()
		go fThread(i)
		m.Lock()
		go sThread(i)
	}
	//time.Sleep(100 * time.Millisecond)
	wg.Wait()
}
func fThread(i int) {
	fmt.Println("from the fThread\t", i)
	m.Unlock()
	wg.Done()
}

func sThread(i int) {
	fmt.Println("from the sThread\t", i)
	m.Unlock()
	wg.Done()
}
