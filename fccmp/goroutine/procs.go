package main

import (
	"fmt"
	"runtime"
)

func main() {
	runtime.GOMAXPROCS(30)
	fmt.Println("no of threads", runtime.GOMAXPROCS(-1))
}
