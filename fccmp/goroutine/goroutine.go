package main

import (
	"fmt"
	"time"
)

func main() {
	fmt.Println("from the goroutine example")
	go firstFunction()
	time.Sleep(1000 * time.Millisecond)
}

func firstFunction() {
	fmt.Println("from the firstFunction using the goroutine")
}
