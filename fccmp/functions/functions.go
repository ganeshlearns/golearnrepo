package main

import (
	"fmt"
)

func main() {
	fmt.Println("from the function example")
	for i := 0; i <= 5; i++ {
		//first function call
		newFunc("ganesh", i)
	}
	//second function call
	secFunc("first", "second")

	//pointers used in functions
	a, b := "pstring1", "pstring2"
	pFunc(&a, &b)
	fmt.Println("in main---after changing value of a with a pointer in the function", a, b)

	//variadic parameters using a return type and pointers
	sum := variFunc("ganesh", 1, 2, 3, 4, 5, 6, 7, 8)
	fmt.Println("the sum returned from the function is ", *sum)
}

//function declarations

func newFunc(msg string, index int) {
	fmt.Println("from the newFunc(msg,index )\t", msg, index)
}

func secFunc(farg, secarg string) {
	fmt.Println("from the secFunc(farg,secarg)\t", farg, secarg)
}

func pFunc(farg, secarg *string) {
	fmt.Println("from the pFunc(farg,secarg *string)\t", *farg, secarg)
	*farg = "pnewstring"
	fmt.Println("after the pointer is used to change the value of farg", *farg)
}

func variFunc(msg string, values ...int) *int {
	sum := 0
	fmt.Println(msg, values)
	for k, v := range values {
		fmt.Println(k, v)
		sum = sum + v
	}
	fmt.Println(sum)
	return &sum
}
