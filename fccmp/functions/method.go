package main

import (
	"fmt"
)

func main() {
	fmt.Println("from the method example")
	ms := mystruct{
		number: 199,
	}
	ms.mfun()
}

//struct declaration
type mystruct struct {
	number int
}

//method declaration
func (m mystruct) mfun() {
	fmt.Println(m.number)
}
