package main

import (
	"fmt"
)

func main() {
	result, err := divide(3, 5)
	if err != nil {
		//err1 := recover()
		fmt.Println(err)
		//recover()
		//panic("this is an error")
		return
	}
	fmt.Println("result from the main", result)
}
func divide(a, b float64) (float64, error) {
	if b == 0 {
		return 0.0, fmt.Errorf("cant divide by zero")
	}
	return a / b, nil
}
