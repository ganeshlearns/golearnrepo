package main

import (
	"fmt"
	"time"
)

func main() {
	d, _ := time.ParseDuration("10ms")
	fmt.Println("from the time formatting example main", time.Now())
	fmt.Println(d)
}
