package main

import (
	"fmt"
	"m1/mpack"
	"m1/mpack2"
)

func main() {
	fmt.Println("from the main() in mpack package")
	mpack.FstFunc()
	mpack2.FstFunc()

}
