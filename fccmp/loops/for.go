package main

import (
	"fmt"
)

func main() {
	fmt.Println("from the for loop example")

	//simple for loop
	fmt.Println("###########################")
	fmt.Println("simple for loop")
	for i := 0; i < 5; i++ {
		fmt.Println("printing i", i)
	}

	//other form of  for loop
	fmt.Println("###########################")
	fmt.Println("other form of  for loop")
	j := 0
	for j < 5 {
		fmt.Println("printing j", j)
		j++
	}

	//for loop which uses break to get out of the loop
	fmt.Println("###########################")
	fmt.Println("for loop which uses break to get out of the loop")
	p := 0
	for {
		fmt.Println("printing p", p)
		p++
		if p >= 5 {
			break
		}
	}

	//for loop which uses continue
	fmt.Println("###########################")
	fmt.Println("for loop which uses continue")

	for q := 0; q <= 5; q++ {

		if q == 3 {
			continue
		}
		fmt.Println("printing q", q)

	}

	//for loop which uses break with a label
	fmt.Println("###########################")
	fmt.Println("for loop which uses break with a label")

label1:

	for i := 0; i < 3; i++ {
		fmt.Println("printing i\t", i)
		for q := 0; q <= 5; q++ {

			if q == 3 {
				break label1
			}
			fmt.Println("printing q", q)

		}
	}

	//for loop which uses range for arrays ,slices ,maps
	fmt.Println("###########################")
	fmt.Println("for loop which uses range for arrays ,slices ,maps")

	arr1 := []int{11, 12, 13, 14, 15}
	for k, v := range arr1 {

		fmt.Println("printing v", k, v)

	}

}
