//package declaration
package main

//importing packages
import (
	"fmt"
	"strconv"
)

//main function
func main() {
	//declaring a variable
	var s string
	//implicit declaration and initialization
	k := 300
	s = strconv.Itoa(k)
	//printing the variable
	fmt.Printf("%v , %T\n", k, k)
	fmt.Printf("%v , %T\n", s, s)
}
