//package declaration
package main

//importing packages
import "fmt"

//declaration here
var m int = 400

//block declaration of variables
var (
	n int = 500
)

//main function
func main() {
	//declaring a variable
	var i int
	//assigning a value to the variable
	i = 100
	//declaring a variable and initializing simultaneously
	var j int = 200
	//implicit declaration and initialization
	k := 300
	//printing the variable
	fmt.Printf("%v , %T\n", i, i)
	fmt.Printf("%v , %T\n", j, j)
	fmt.Println(k)
	fmt.Println(m)
	fmt.Println(n)
}
