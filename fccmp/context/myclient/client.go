package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
)

func clientMain() {
	fmt.Println("from the client")
	//response from the URL used
	resp, err := http.Get("http://localhost:8080/hello")
	if err != nil {
		log.Fatal(err)
	}
	//body of response is read and printed
	body, err := io.ReadAll(resp.Body)
	fmt.Println(string(body))
}

func main() {
	clientMain()
}
