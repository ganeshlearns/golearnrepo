package main

import (
	"fmt"
	"log"
	"net/http"
)

//handler is implemented here
func reqHandler(rw http.ResponseWriter, req *http.Request) {
	fmt.Println("from the reqHandler func")

	//written to the response writer here
	_, err := rw.Write([]byte("body of response"))
	if err != nil {
		log.Fatal(err)
	}

	req, err = http.NewRequest(http.MethodGet, "http://google.com", nil)
	if err != nil {
		log.Fatal(err)
	}

	//ctx:=context.Background()
	ctx := req.Context()
	req.WithContext(ctx)

	resp2, err2 := http.DefaultClient.Do(req)
	if err2 != nil {
		log.Fatal(err2)
	}

	fmt.Println(resp2)

}

func serverMain() {
	fmt.Println("from the server")

	//appropriate handler is called
	http.HandleFunc("/hello", reqHandler)

	//server is listening on port 8080 on localhost
	err := http.ListenAndServe("localhost:8080", nil)
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	serverMain()
}
