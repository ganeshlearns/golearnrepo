package main

import (
	"fmt"
	"sync"
)

//declaring a WaitGroup
var wg sync.WaitGroup

func main() {
	fmt.Println("from the send/receive channel example")
	//making a channel which sends int data with buffered value
	ch := make(chan int, 50)
	//adding no of threads to a waitgroup
	wg.Add(2)
	//first thread
	go func(ch chan<- int) {
		//into the channel
		ch <- 42
		ch <- 34
		close(ch)
		//one of the threads is done here
		wg.Done()
	}(ch)

	go func(ch <-chan int) {
		//data coming from a channel
		for i := range ch {
			fmt.Println(i)
		}
		//one of the threads is done here
		wg.Done()
	}(ch)
	//waitgroup waits for the threads in the waitgroup to finish
	wg.Wait()
}
