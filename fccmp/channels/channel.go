package main

import (
	"fmt"
	"sync"
)

//declaring a WaitGroup
var wg sync.WaitGroup

func main() {
	fmt.Println("from the two way channel example")
	//making a channel which sends int data
	ch := make(chan int)
	//adding no of threads to a waitgroup
	wg.Add(2)
	//first thread
	go func() {
		//into the channel
		ch <- 42
		//data coming from a channel
		i := <-ch
		fmt.Println(i)
		//one of the threads is done here
		wg.Done()
	}()

	go func() {
		//data coming from a channel
		i := <-ch
		fmt.Println(i)
		//into the channel
		ch <- 22
		//one of the threads is done here
		wg.Done()
	}()
	//waitgroup waits for the threads in the waitgroup to finish
	wg.Wait()
}
